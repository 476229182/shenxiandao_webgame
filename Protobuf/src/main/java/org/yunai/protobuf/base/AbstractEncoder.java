package org.yunai.protobuf.base;

import java.util.List;

public abstract class AbstractEncoder {

	public abstract ByteArray encode(Struct struct);
	
	protected void convertByte(ByteArray byteArray, List<Byte> list) {
		byteArray.incrLimit(list.size() + 2);
	}

    protected void putByte(ByteArray byteArray, List<Byte> list) {
		byteArray.putShort((short) list.size());
		for (Byte b : list) {
			byteArray.putByte(b);
		}
	}

    protected void convertChar(ByteArray byteArray, List<Character> list) {
		byteArray.incrLimit(list.size() << 1 + 2);
	}

    protected void putChar(ByteArray byteArray, List<Character> list) {
		byteArray.putShort((short) list.size());
		for (char ch : list) {
			byteArray.putChar(ch);
		}
	}

    protected void convertShort(ByteArray byteArray, List<Short> list) {
		byteArray.incrLimit(list.size() << 1 + 2);
	}

    protected void putShort(ByteArray byteArray, List<Short> list) {
		byteArray.putShort((short) list.size());
		for (short sh : list) {
			byteArray.putShort(sh);
		}
	}

    protected void convertInt(ByteArray byteArray, List<Integer> list) {
		byteArray.incrLimit(list.size() << 2 + 2);
	}

    protected void putInt(ByteArray byteArray, List<Integer> list) {
		byteArray.putShort((short) list.size());
		for (int i : list) {
			byteArray.putInt(i);
		}
	}

    protected void convertLong(ByteArray byteArray, List<Long> list) {
		byteArray.incrLimit(list.size() << 3 + 2);
	}

    protected void putLong(ByteArray byteArray, List<Long> list) {
		byteArray.putShort((short) list.size());
		for (long l : list) {
			byteArray.putLong(l);
		}
	}

    protected void convertFloat(ByteArray byteArray, List<Float> list) {
		byteArray.incrLimit(list.size() << 2 + 2);
	}

    protected void putFloat(ByteArray byteArray, List<Float> list) {
		byteArray.putShort((short) list.size());
		for (float f : list) {
			byteArray.putFloat(f);
		}
	}

    protected void convertDouble(ByteArray byteArray, List<Double> list) {
		byteArray.incrLimit(list.size() << 3 + 2);
	}

    protected void putDouble(ByteArray byteArray, List<Double> list) {
		byteArray.putShort((short) list.size());
		for (double d : list) {
			byteArray.putDouble(d);
		}
	}

    protected byte[] convertString(ByteArray byteArray, String str) {
		byte[] bytes = Bits.convertString(str);
		byteArray.incrLimit(bytes.length + 2);
		return bytes;
	}

    protected byte[][] convertStringList(ByteArray byteArray, List<String> strs) {
		byteArray.incrLimit(2);
		byte[][] bytes = new byte[strs.size()][];
		short index = 0;
		for (String str : strs) {
			bytes[index++] = convertString(byteArray, str);
		}
		return bytes;
	}

    protected void putString(ByteArray byteArray, byte[] bytes) {
		byteArray.putShort((short) bytes.length);
		byteArray.putByte(bytes);
	}

    protected void putStringList(ByteArray byteArray, byte[][] bytes) {
		byteArray.putShort((short) bytes.length);
		for (byte[] byteZ : bytes) {
			putString(byteArray, byteZ);
		}
	}

    protected byte[] convertMessage(ByteArray byteArray, AbstractEncoder encoder, Struct struct) {
		ByteArray newByteArray = encoder.encode(struct);
		byteArray.incrLimit(newByteArray.limit());
		return newByteArray.getHb();
	}

    protected <T extends Struct> byte[][] convertMessageList(ByteArray byteArray, AbstractEncoder encoder, List<T> messages) {
		byteArray.incrLimit(2);
		byte[][] bytes = new byte[messages.size()][];
		short index = 0;
		for (Struct struct : messages) {
			bytes[index++] = convertMessage(byteArray, encoder, struct);
		}
		return bytes;
	}

    protected void putMessage(ByteArray byteArray, byte[] bytes) {
		byteArray.putByte(bytes);
	}

    protected void putMessageList(ByteArray byteArray, byte[][] bytes) {
		byteArray.putShort((short) bytes.length);
		for (byte[] byteZ : bytes) {
			putMessage(byteArray, byteZ);
		}
	}
}