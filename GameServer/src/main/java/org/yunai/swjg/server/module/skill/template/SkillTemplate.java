package org.yunai.swjg.server.module.skill.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.skill.SkillDef;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.*;

/**
 * 技能模版
 * User: yunai
 * Date: 13-6-11
 * Time: 下午12:34
 */
public class SkillTemplate {

    private static final String KEY_SKILL_IMPACT_BASE = "skillImpact";
    private static final int SKILL_IMPACT_MAX = 2;

    private int sn;
    private String name;
    private int needMorale;
    private int costMorale;
    private SkillDef.Type skillType;
    private int skillFactor;
    private SkillDef.Scope scope;
    private SkillDef.Target target;
    private int maxTarget;
    private SkillDef.TargetSort targetSort;
    private List<SkillImpactInfoVO> impacts;
    /**
     * 攻击前效果
     */
    private List<SkillImpactInfoVO> beforeImpacts;
    /**
     * 攻击后效果
     */
    private List<SkillImpactInfoVO> afterImpacts;

    public int getSn() {
        return sn;
    }

    public String getName() {
        return name;
    }

    public int getNeedMorale() {
        return needMorale;
    }

    public int getCostMorale() {
        return costMorale;
    }

    public SkillDef.Type getSkillType() {
        return skillType;
    }

    public int getSkillFactor() {
        return skillFactor;
    }

    public SkillDef.Scope getScope() {
        return scope;
    }

    public SkillDef.Target getTarget() {
        return target;
    }

    public int getMaxTarget() {
        return maxTarget;
    }

    public SkillDef.TargetSort getTargetSort() {
        return targetSort;
    }

    public List<SkillImpactInfoVO> getImpacts() {
        return impacts;
    }

    public List<SkillImpactInfoVO> getBeforeImpacts() {
        return beforeImpacts;
    }

    public List<SkillImpactInfoVO> getAfterImpacts() {
        return afterImpacts;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, SkillTemplate> templates;

    public static SkillTemplate getItemTemplate(Integer sn) {
        return templates.get(sn);
    }

    public static void load() {
        CsvReader reader = null;
        // 道具公用模板
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/skill/skill_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                SkillTemplate template = genSkillTemplate(reader);
                templates.put(template.getSn(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    private static SkillTemplate genSkillTemplate(CsvReader reader) throws IOException {
        SkillTemplate template = new SkillTemplate();
        template.sn = CsvUtil.getInt(reader, "sn", 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.needMorale = CsvUtil.getInt(reader, "needMorale", 0);
        template.costMorale = CsvUtil.getInt(reader, "costMorale", 0);
        template.skillType = SkillDef.Type.valueOf(CsvUtil.getInt(reader, "skillType", 0));
        template.skillFactor = CsvUtil.getInt(reader, "skillFactor", 0);
        template.scope = SkillDef.Scope.valueOf(CsvUtil.getInt(reader, "scope", 0));
        template.target = SkillDef.Target.valueOf(CsvUtil.getInt(reader, "target", 0));
        template.maxTarget = CsvUtil.getInt(reader, "maxTarget", 0);
        template.targetSort = SkillDef.TargetSort.valueOf(CsvUtil.getInt(reader, "targetSort", 0));
        template.impacts = new ArrayList<>(SKILL_IMPACT_MAX);
        template.beforeImpacts = new ArrayList<>(SKILL_IMPACT_MAX);
        template.afterImpacts = new ArrayList<>(SKILL_IMPACT_MAX);
        for (int i = 1; i <= SKILL_IMPACT_MAX; i++) {
            if (CsvUtil.getString(reader, KEY_SKILL_IMPACT_BASE + i, "").equals("")) {
                break;
            }
            SkillImpactInfoVO impact = SkillImpactInfoVO.parse(CsvUtil.getString(reader, KEY_SKILL_IMPACT_BASE + i, ""));
            template.impacts.add(impact);
            switch (impact.getTime()) {
                case BEFORE_ATTACK:
                    template.beforeImpacts.add(impact);
                    break;
                case AFTER_ATTACK:
                    template.afterImpacts.add(impact);
                    break;
            }
        }
        template.impacts = Collections.unmodifiableList(template.impacts);
        template.beforeImpacts = Collections.unmodifiableList(template.beforeImpacts);
        template.afterImpacts = Collections.unmodifiableList(template.afterImpacts);
        return template;
    }

}
