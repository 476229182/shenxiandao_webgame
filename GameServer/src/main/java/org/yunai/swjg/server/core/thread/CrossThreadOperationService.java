package org.yunai.swjg.server.core.thread;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.yfserver.async.IIoOperationService;

import javax.annotation.Resource;

/**
 * 跨服务操作服务
 * User: yunai
 * Date: 13-5-21
 * Time: 下午8:01
 */
@Service
public class CrossThreadOperationService {

    @Resource
    private IIoOperationService ioOperationService;
    @Resource
    private OnlineContextService onlineContextService;

    public void execute(AbstractOtherPlayerOperation otherPlayerOperation) {
        Online online = onlineContextService.getPlayer(otherPlayerOperation.getPlayerId());
        if (online != null) {
            online.putMessage(new SysOtherPlayerOperationMessage(otherPlayerOperation));
        } else {
            ioOperationService.asyncExecute(new OfflineOtherPlayerOperation(otherPlayerOperation));
        }
    }
}
