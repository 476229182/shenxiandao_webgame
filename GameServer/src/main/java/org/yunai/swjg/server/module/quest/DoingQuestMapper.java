package org.yunai.swjg.server.module.quest;

import org.yunai.swjg.server.entity.DoingQuestEntity;
import org.yunai.yfserver.persistence.orm.mybatis.DeleteMapper;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

import java.util.List;

/**
 * DoingQuest数据访问接口
 * User: yunai
 * Date: 13-5-9
 * Time: 下午10:47
 */
public interface DoingQuestMapper extends Mapper, SaveMapper<DoingQuestEntity>, DeleteMapper<DoingQuestEntity> {

    /**
     * 获得一个玩家的进行中的任务列表
     *
     * @param playerId 玩家编号
     * @return 玩家进行中的任务列表
     */
    List<DoingQuestEntity> selectList(Integer playerId);

    /**
     * 插入数据
     *
     * @param entity 数据
     */
    @Override
    void insert(DoingQuestEntity entity);

    /**
     * 删除数据
     *
     * @param entity 数据
     */
    @Override
    void delete(DoingQuestEntity entity);
}