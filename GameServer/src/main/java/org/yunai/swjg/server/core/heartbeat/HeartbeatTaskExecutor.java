package org.yunai.swjg.server.core.heartbeat;

import org.yunai.yfserver.common.HeartBeatable;

/**
 * 心跳任务执行器
 * User: yunai
 * Date: 13-5-24
 * Time: 上午9:33
 */
public interface HeartbeatTaskExecutor extends HeartBeatable {

    /**
     * 提交心跳任务
     *
     * @param task 任务
     */
    void submit(HeartbeatTask task);

//    /**
//     * 清理所有心跳任务
//     */
//    void clear();
}
