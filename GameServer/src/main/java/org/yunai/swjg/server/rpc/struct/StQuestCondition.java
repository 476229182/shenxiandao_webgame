package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 任务完成条件结构体
 */
public class StQuestCondition implements IStruct {
    /**
     * 条件类型
     */
    private Byte type;
    /**
     * 条件主体
     */
    private Integer subject;
    /**
     * 主体需要数量
     */
    private Integer count;
    /**
     * 主体完成数量
     */
    private Integer doCount;

    public StQuestCondition() {
    }

    public StQuestCondition(Byte type, Integer subject, Integer count, Integer doCount) {
        this.type = type;
        this.subject = subject;
        this.count = count;
        this.doCount = doCount;
    }

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}
	public Integer getSubject() {
		return subject;
	}

	public void setSubject(Integer subject) {
		this.subject = subject;
	}
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getDoCount() {
		return doCount;
	}

	public void setDoCount(Integer doCount) {
		this.doCount = doCount;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StQuestCondition struct = new StQuestCondition();
            struct.setType(byteArray.getByte());
            struct.setSubject(byteArray.getInt());
            struct.setCount(byteArray.getInt());
            struct.setDoCount(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StQuestCondition struct = (StQuestCondition) message;
            ByteArray byteArray = ByteArray.createNull(13);
            byteArray.create();
            byteArray.putByte(struct.getType());
            byteArray.putInt(struct.getSubject());
            byteArray.putInt(struct.getCount());
            byteArray.putInt(struct.getDoCount());
            return byteArray;
        }
    }
}