package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 道具实体
 * User: yunai
 * Date: 13-4-6
 * Time: 上午11:38
 */
public class ItemEntity implements Entity<Integer> {

    /**
     * ID主键
     */
    private int id;
    /**
     * 所属玩家ID
     */
    private int playerId;
    /**
     * 佩带者ID
     */
    private int wearId;
    /**
     * 物品所在背包编号
     */
    private int bagId;
    /**
     * 物品所在背包位置
     */
    private int bagIndex;
    /**
     * 物品类型编号
     */
    private int templateId;
    /**
     * 叠加数量
     */
    private int overlap;
    /**
     * 物品自身的属性
     */
    private String properties;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getWearId() {
        return wearId;
    }

    public void setWearId(int wearId) {
        this.wearId = wearId;
    }

    public int getBagId() {
        return bagId;
    }

    public void setBagId(int bagId) {
        this.bagId = bagId;
    }

    public int getBagIndex() {
        return bagIndex;
    }

    public void setBagIndex(int bagIndex) {
        this.bagIndex = bagIndex;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public int getOverlap() {
        return overlap;
    }

    public void setOverlap(int overlap) {
        this.overlap = overlap;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }
}
