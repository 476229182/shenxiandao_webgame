package org.yunai.swjg.server.module.monster.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.rep.template.RepTemplate;
import org.yunai.yfserver.util.CollectionUtils;
import org.yunai.yfserver.util.CsvUtil;
import org.yunai.yfserver.util.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * 可见怪物模版
 * User: yunai
 * Date: 13-5-17
 * Time: 下午3:13
 */
public class VisibleMonsterTemplate {

    /**
     * 怪物分组信息
     */
    public static class MonsterGroup {

        /**
         * 怪物模版
         */
        private MonsterTemplate template;
        /**
         * 阵形位置
         */
        private Short position;

        public MonsterGroup(MonsterTemplate template, Short position) {
            this.template = template;
            this.position = position;
        }

        public MonsterTemplate getTemplate() {
            return template;
        }

        public void setTemplate(MonsterTemplate template) {
            this.template = template;
        }

        public Short getPosition() {
            return position;
        }

        public void setPosition(Short position) {
            this.position = position;
        }
    }

    /**
     * 编号
     */
    private Integer id;
    /**
     * 名字
     */
    private String name;
    /**
     * 副本模版
     */
    private RepTemplate repTemplate;
    /**
     * 怪物组列表
     */
    private List<MonsterGroup> monsterGroups;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RepTemplate getRepTemplate() {
        return repTemplate;
    }

    public void setRepTemplate(RepTemplate repTemplate) {
        this.repTemplate = repTemplate;
    }

    public List<MonsterGroup> getMonsterGroups() {
        return monsterGroups;
    }

    public void setMonsterGroups(List<MonsterGroup> monsterGroups) {
        this.monsterGroups = monsterGroups;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, List<VisibleMonsterTemplate>> templates;

    public static List<VisibleMonsterTemplate> get(Integer repId) {
        return templates.get(repId);
    }

    public static void load() {
        CsvReader reader = null;
        // 怪物模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/monster/visible_monster_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                VisibleMonsterTemplate template = read(reader);
                if (templates.containsKey(template.getRepTemplate().getId())) {
                    templates.get(template.getRepTemplate().getId()).add(template);
                } else {
                    templates.put(template.getRepTemplate().getId(), CollectionUtils.asList(template));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static VisibleMonsterTemplate read(CsvReader reader) throws IOException {
        VisibleMonsterTemplate template = new VisibleMonsterTemplate();
        template.id  = CsvUtil.getInt(reader, "id", 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.repTemplate = RepTemplate.get(CsvUtil.getInt(reader, "repId", 0));
        template.monsterGroups = readMonsters(reader);
        return template;
    }

    private static List<MonsterGroup> readMonsters(CsvReader reader) throws IOException {
        String[] monsters = StringUtils.split(CsvUtil.getString(reader, "monsterData", ""), "\\|");
        if (monsters.length == 0) {
            return Collections.emptyList();
        }
        List<MonsterGroup> monsterGroups = new ArrayList<>(monsters.length);
        for (String monsterStr : monsters) {
            int[] monsterGroup = StringUtils.splitInt(monsterStr, "\\:");
            MonsterGroup group = new MonsterGroup(MonsterTemplate.get(monsterGroup[1]), (short) monsterGroup[0]);
            monsterGroups.add(group);
        }
        return monsterGroups;
    }
}
