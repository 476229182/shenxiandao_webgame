package org.yunai.swjg.server.module.scene.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.scene.NormalSceneService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_SceneMoveReq;

import javax.annotation.Resource;

/**
 * 玩家场景移动命令
 * User: yunai
 * Date: 13-5-4
 * Time: 下午5:08
 */
@Component
public class NormalSceneMoveCommand extends GameMessageCommand<C_S_SceneMoveReq> {

    @Resource
    private NormalSceneService sceneService;

    @Override
    public void execute(Online online, C_S_SceneMoveReq msg) {
        sceneService.onPlayerMoveNormalScene(online, msg.getFromX(), msg.getFromY(), msg.getToSceneX(), msg.getToSceneY());
    }
}