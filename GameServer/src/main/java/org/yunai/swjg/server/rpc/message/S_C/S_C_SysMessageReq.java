package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【30000】: 系统消息请求
 */
public class S_C_SysMessageReq extends GameMessage {
    public static final short CODE = 30000;

    /**
     * 系统消息枚举编号
     */
    private Integer id;

    public S_C_SysMessageReq() {
    }

    public S_C_SysMessageReq(Integer id) {
        this.id = id;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_SysMessageReq struct = new S_C_SysMessageReq();
            struct.setId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_SysMessageReq struct = (S_C_SysMessageReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getId());
            return byteArray;
        }
    }
}