package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 玩家结构体
 */
public class StMember implements IStruct {
    /**
     * 昵称
     */
    private String nickname;

    public StMember() {
    }

    public StMember(String nickname) {
        this.nickname = nickname;
    }

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StMember struct = new StMember();
            struct.setNickname(getString(byteArray));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StMember struct = (StMember) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            putString(byteArray, nicknameBytes);
            return byteArray;
        }
    }
}