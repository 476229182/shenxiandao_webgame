package org.yunai.swjg.server.module.item.operation.use;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.module.currency.Currency;
import org.yunai.swjg.server.module.currency.CurrencyService;
import org.yunai.swjg.server.module.item.Inventory;
import org.yunai.swjg.server.module.item.ItemDef;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.container.ShoulderBag;
import org.yunai.swjg.server.module.item.template.ItemTemplate;
import org.yunai.swjg.server.module.item.template.MaterialItemTemplate;
import org.yunai.swjg.server.module.item.template.ReelItemTemplate;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_ItemUseReelResp;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;
import org.yunai.yfserver.spring.BeanManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 丹药合成卷轴使用操作
 * User: yunai
 * Date: 13-6-4
 * Time: 下午6:45
 */
@Component
public class UseMedicineReelOperation extends AbstractItemUseOperation {

    private static CurrencyService currencyService;

    static {
        currencyService = BeanManager.getBean(CurrencyService.class);
    }

    @Override
    public boolean isSuitable(Player player, Item item, int targetId) {
        return item.getBagType() == Bag.BagType.PRIM
                && item.getTemplate().getType() == ItemDef.Type.MEDICINE_REEL;
    }

    /**
     * 判断合成材料是否足够。合成材料来源有主背包/仓库背包。
     *
     * @param player       玩家信息
     * @param item         道具
     * @param targetId     无用参数
     * @param useGoldParam 是否使用元宝参数
     * @return 材料是否足够
     */
    @Override
    protected boolean canUseImpl(Player player, Item item, int targetId, int useGoldParam) {
        ReelItemTemplate template = (ReelItemTemplate) item.getTemplate();
        boolean result = true;
        boolean useGold = (useGoldParam == 1);
        int needGold = 0; // 需要元宝
        // 第一步，检查
        ShoulderBag primBag = player.getInventory().getPrimBag();
        ShoulderBag deportBag = player.getInventory().getDeportBag();
        for (Map.Entry<Integer, Integer> needItem : template.getNeedItems().entrySet()) {
            int ownCount = primBag.getCountByTemplateId(needItem.getKey()); // 主背包
            if (ownCount >= needItem.getValue()) {
                continue;
            }
            ownCount += deportBag.getCountByTemplateId(needItem.getKey()); // 主背包 + 仓库
            if (ownCount >= needItem.getValue()) {
                continue;
            }
            if (!useGold) { // 道具不够，又不花钱
                result = false;
                break;
            }
            needGold += ItemTemplate.getItemTemplate(needItem.getKey(), MaterialItemTemplate.class).getGold() * (needItem.getValue() - ownCount);
            if (!currencyService.canCost(player, Currency.GOLD, needGold, false)) { // 道具不够，花的钱又不够
                result = false;
                break;
            }
        }
        // 最后，发送结果消息
        if (!result) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_ENOUGH));
            player.message(new S_C_ItemUseReelResp((byte) 0));
            return false;
        }
        return true;
    }

    /**
     * 合成丹药。
     *
     * @param player       玩家信息
     * @param item         道具
     * @param targetId     无用参数
     * @param useGoldParam 是否使用元宝参数
     * @return
     */
    @Override
    protected boolean useImpl(Player player, Item item, int targetId, int useGoldParam) {
        ReelItemTemplate template = (ReelItemTemplate) item.getTemplate();
        List<Item> updateItems = new ArrayList<>();
        int needGold = 0;
        // 第一步，扣除材料道具
        Inventory inventory = player.getInventory();
        ShoulderBag primBag = inventory.getPrimBag();
        ShoulderBag deportBag = inventory.getDeportBag();
        for (Map.Entry<Integer, Integer> needItem : template.getNeedItems().entrySet()) {
            int count = needItem.getValue();
            count = consumeItem(primBag, needItem.getKey(), count, updateItems);
            if (count > 0) {
                count = consumeItem(deportBag, needItem.getKey(), count, updateItems);
            }
            if (count > 0) {
                needGold += ItemTemplate.getItemTemplate(needItem.getKey(), MaterialItemTemplate.class).getGold() * count;
            }
        }
        // 第三步，扣除元宝
        if (needGold > 0) {
            currencyService.cost(player, Currency.GOLD, needGold);
        }
        // 第四步，扣除丹药合成卷轴
        item.changeOverlap(0);
        updateItems.add(item);
        // 第五步，给丹药
        updateItems.addAll(primBag.addItem(template.getGainTemplate(), template.getGainCount()));
        // 发消息
        inventory.sendModifyMessage(updateItems); // 发送背包变化
        player.message(new S_C_ItemUseReelResp((byte) 1)); // 发送合成结果
        return true;
    }
}