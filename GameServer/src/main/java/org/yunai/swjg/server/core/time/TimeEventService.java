package org.yunai.swjg.server.core.time;

import org.yunai.yfserver.schedule.ScheduleService;
import org.yunai.yfserver.time.TimeService;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-5-24
 * Time: 下午4:07
 */
public class TimeEventService {

    /**
     * 时间服务
     */
    private final TimeService timeService;
    /**
     * 调度任务管理器
     */
    private final ScheduleService scheduleService;

    public TimeEventService(TimeService timeService, ScheduleService scheduleService) {
        this.timeService = timeService;
        this.scheduleService = scheduleService;
    }
}
