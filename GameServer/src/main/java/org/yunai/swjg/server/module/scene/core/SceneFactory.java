package org.yunai.swjg.server.module.scene.core;

import org.yunai.swjg.server.module.rep.ActivityRepScene;
import org.yunai.swjg.server.module.rep.RepScene;
import org.yunai.swjg.server.module.rep.template.RepTemplate;
import org.yunai.swjg.server.module.scene.NormalScene;
import org.yunai.swjg.server.module.scene.core.template.SceneTemplate;

/**
 * 场景工厂类
 * User: yunai
 * Date: 13-4-22
 * Time: 下午11:52
 */
public class SceneFactory {

    private SceneFactory() {}

    public static AbstractScene createScene(SceneTemplate sceneTemplate) {
        if (sceneTemplate.getType() == SceneDef.Type.NORMAL) {
            NormalScene scene = new NormalScene(sceneTemplate);
            return scene;
        }
        if (sceneTemplate.getType() == SceneDef.Type.REP) {
            RepTemplate repTemplate = RepTemplate.getBySceneId(sceneTemplate.getId());
            RepScene rep = new RepScene(repTemplate);
            rep.init();
            return rep;
        }
        if (sceneTemplate.getType() == SceneDef.Type.ACTIVITY) {
            RepTemplate repTemplate = RepTemplate.getBySceneId(sceneTemplate.getId());
            ActivityRepScene rep = new ActivityRepScene(repTemplate);
            rep.init();
            return rep;
        }
        throw new RuntimeException("数据错误，不支持该场景类型: " + sceneTemplate.getId());
    }
}
