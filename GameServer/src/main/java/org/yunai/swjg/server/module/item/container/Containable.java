package org.yunai.swjg.server.module.item.container;

/**
 * 可以放在Container中的物品<br />
 * 同类型的可以叠放在一起，但有堆叠上限，占据Container的一个index
 * User: yunai
 * Date: 13-4-6
 * Time: 上午10:34
 */
public interface Containable {
    /**
     * 所属容器类型
     *
     * @return 如果还没分配给某容器，返回{@link BagType#NULL}
     */
    Bag.BagType getBagType();

    /**
     * 在所属容器中的索引
     *
     * @return 如果还没分配给某容器，返回-1
     */
    int getIndex();

    /**
     * 堆叠数量
     *
     * @return
     */
    int getOverlap();

    /**
     * 最大堆叠数量
     *
     * @return
     */
    int getMaxOverlap();
}