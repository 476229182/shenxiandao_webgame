package org.yunai.swjg.server.core;

import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * 服务器
 * User: yunai
 * Date: 13-3-12
 * Time: 下午2:18
 */
public class GameServer {

    private NioSocketAcceptor acceptor;

    private Integer port;

    public void setAcceptor(NioSocketAcceptor acceptor) {
        this.acceptor = acceptor;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * 启动服务器
     * @throws java.io.IOException
     */
    public void start() throws IOException {
        acceptor.bind(new InetSocketAddress(port));
    }

    /**
     * 关闭服务器
     */
    public void stop() {
        acceptor.unbind();
    }
}
