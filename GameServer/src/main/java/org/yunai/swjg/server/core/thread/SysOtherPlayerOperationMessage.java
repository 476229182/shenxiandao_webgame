package org.yunai.swjg.server.core.thread;

import org.yunai.yfserver.message.sys.SysInternalMessage;

/**
 * 对在线的其他玩家发出操作的消息<br />
 * TODO 该消息如果在玩家消息队列里时，下线的时候也要执行掉！
 * User: yunai
 * Date: 13-5-22
 * Time: 上午12:38
 */
public class SysOtherPlayerOperationMessage extends SysInternalMessage {

    private final AbstractOtherPlayerOperation operation;

    public SysOtherPlayerOperationMessage(AbstractOtherPlayerOperation operation) {
        this.operation = operation;
    }

    @Override
    public void execute() {
        operation.doOnline();
    }

    @Override
    public short getCode() {
        return 0;
    }
}
