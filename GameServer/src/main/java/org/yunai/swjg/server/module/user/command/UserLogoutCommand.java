package org.yunai.swjg.server.module.user.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.rpc.message.C_S.C_S_LogoutReq;
import org.yunai.swjg.server.rpc.message.S_C.S_C_LogoutResp;

/**
 * 用户登出命令
 * User: yunai
 * Date: 13-3-30
 * Time: 下午2:35
 */
@Component
public class UserLogoutCommand extends GameMessageCommand<C_S_LogoutReq> {

    @Override
    public void execute(Online online, C_S_LogoutReq msg) {
        online.write(new S_C_LogoutResp());
        online.disconnect();
    }
}