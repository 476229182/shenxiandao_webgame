package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 键值对结构体(值类型为String)
 */
public class StKeyValuePairString implements IStruct {
    /**
     * 键
     */
    private Integer key;
    /**
     * 值
     */
    private String val;

    public StKeyValuePairString() {
    }

    public StKeyValuePairString(Integer key, String val) {
        this.key = key;
        this.val = val;
    }

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}
	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StKeyValuePairString struct = new StKeyValuePairString();
            struct.setKey(byteArray.getInt());
            struct.setVal(getString(byteArray));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StKeyValuePairString struct = (StKeyValuePairString) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byte[] valBytes = convertString(byteArray, struct.getVal());
            byteArray.create();
            byteArray.putInt(struct.getKey());
            putString(byteArray, valBytes);
            return byteArray;
        }
    }
}