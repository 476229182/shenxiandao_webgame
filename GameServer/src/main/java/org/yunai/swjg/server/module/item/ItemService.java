package org.yunai.swjg.server.module.item;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.container.ShoulderBag;
import org.yunai.swjg.server.module.item.template.ItemTemplate;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;
import org.yunai.yfserver.util.Assert;

import java.util.List;

/**
 * 道具Service
 * User: yunai
 * Date: 13-4-11
 * Time: 下午10:50
 */
@Service
public class ItemService {

    /**
     * 添加道具<br />
     * TODO 该方法用于测试用，实际需要修改
     *
     * @param online     在线信息
     * @param templateId 道具模版编号
     * @param count      道具数量
     */
    public void addItemToPrimBag(Online online, Integer templateId, Integer count) {
        ItemTemplate template = ItemTemplate.getItemTemplate(templateId);
        Assert.notNull(template, "templateId:" + templateId + "不存在！");

        ShoulderBag primBag = online.getPlayer().getInventory().getPrimBag();
        List<Item> updateItems = primBag.addItem(template, count);
        if (!updateItems.isEmpty()) {
            online.getPlayer().getInventory().sendModifyMessage(updateItems);
        }
    }

    /**
     * 丢弃道具<br />
     * 只允许主背包({@link org.yunai.swjg.server.module.item.container.Bag.BagType#PRIM}丢弃
     * TODO 该接口未整理
     *
     * @param online 在线信息
     * @param bagId  背包标号
     * @param index  背包位置
     */
    public void dropItem(Online online, byte bagId, int index) {
        Bag.BagType bagType = Bag.BagType.valueOf(bagId);
        if (Bag.BagType.isNullBag(bagType)) {
            return;
        }
        CommonBag bag = online.getPlayer().getInventory().getBag(bagType);
        if (bag == null) {
            return;
        }

        Item updateItem;
        switch (bagType) {
            case PRIM:
                updateItem = bag.drop(index);
                break;
            default:
                throw new RuntimeException("error bag");
        }
        if (updateItem != null) {
            online.getPlayer().getInventory().sendModifyMessage(updateItem);
        }
    }

    /**
     * 背包整理<br />
     * 允许整理的背包有：
     * <pre>
     *     1. 主背包({@link org.yunai.swjg.server.module.item.container.Bag.BagType#PRIM}
     *     2. 仓库背包({@link org.yunai.swjg.server.module.item.container.Bag.BagType#DEPOT}
     * </pre>
     *
     * @param online 在线信息
     * @param bagId  背包编号
     */
    public void packUpBag(Online online, byte bagId) {
        Bag.BagType bagType = Bag.BagType.valueOf(bagId);
        if (Bag.BagType.isNullBag(bagType)) {
            return;
        }
        CommonBag bag = online.getPlayer().getInventory().getBag(bagType);
        if (bag == null) {
            return;
        }

        switch (bagType) {
            case PRIM:
            case DEPOT:
                bag.packUp();
                break;
            default:
                throw new RuntimeException("error bag packUp");
        }

        List<Item> items = bag.getAll();
        if (!items.isEmpty()) {
            online.getPlayer().getInventory().sendBagMessage(bag);
        }
    }

    /**
     * 移动道具<br />
     *
     * @param online       在线信息
     * @param fromWearId   移动道具携带者编号
     * @param fromBagId    移动道具所在背包编号
     * @param fromBagIndex 移动道具所在背包位置
     * @param toWearId     目标道具携带者编号
     * @param toBagId      目标背包编号
     * @param toBagIndex   目标背包位置
     */
    public void moveItem(Online online, int fromWearId, byte fromBagId, int fromBagIndex,
                         int toWearId, byte toBagId, int toBagIndex) {
        Player player = online.getPlayer();
        // 检查
        Bag.BagType fromBagType = Bag.BagType.valueOf(fromBagId);
        if (Bag.BagType.isNullBag(fromBagType)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_TYPE_ERROR));
            return;
        }
        Bag.BagType toBagType = Bag.BagType.valueOf(toBagId);
        if (Bag.BagType.isNullBag(toBagType)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_TYPE_ERROR));
            return;
        }
        CommonBag fromBag = player.getInventory().getBag(fromWearId, fromBagType);
        if (fromBag == null) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_TYPE_ERROR));
            return;
        }
        Item item = fromBag.getByIndex(fromBagIndex);
        if (Item.isEmpty(item)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_FOUND));
            return;
        }
        CommonBag toBag = player.getInventory().getBag(toWearId, toBagType);
        if (toBag == null) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_TYPE_ERROR));
            return;
        }
        // 逻辑真正开始
        item.move(toBag, toBagIndex);
    }

    public void useItem(Online online, byte bagId, int bagIndex, int wearId, int targetId, int param) {
        Bag.BagType bagType = Bag.BagType.valueOf(bagId);
        Player player = online.getPlayer();
        if (Bag.BagType.isNullBag(bagType)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_TYPE_ERROR));
            return;
        }
        Inventory inventory = player.getInventory();
        Item item;
        switch (bagType) {
            case PRIM:
                item = inventory.getItem(player.getId(), bagType, bagIndex);
                break;
            case EQUIPMENT:
                item = inventory.getItem(wearId, bagType, bagIndex);
                break;
            default:
                player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_FOUND));
                return;
        }
        if (Item.isEmpty(item)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_FOUND));
            return;
        }
        item.use(targetId, param);
    }
}