package org.yunai.swjg.server.module.quest.event;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.event.ItemCountChangeEvent;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.swjg.server.module.quest.QuestDiary;
import org.yunai.swjg.server.module.quest.vo.DoingQuest;
import org.yunai.yfserver.event.IEventListener;

import java.util.List;

/**
 * 任务模块对于{@link ItemCountChangeEvent}事件的监听器
 * User: yunai
 * Date: 13-5-11
 * Time: 上午1:26
 */
@Component
public class QuestItemCountChangeEventListener implements IEventListener<ItemCountChangeEvent> {

    @Override
    public void fireEvent(ItemCountChangeEvent event) {
        // 判断是否是主背包道具数量变化
        Item item = event.getItem();
        if (item.getBagType() != Bag.BagType.PRIM) {
            return;
        }
        // 获得会受到影响的任务列表
        Player player = event.getPlayer();
        QuestDiary questDiary = player.getQuestDiary();
        Integer templateId = item.getTemplateId();
        List<DoingQuest> doingQuests = questDiary.getDoingQuests(QuestDef.Condition.ITEM, templateId);
        if (doingQuests.isEmpty()) {
            return;
        }
        Integer count = player.getInventory().getPrimBag().getCountByTemplateId(templateId);
        for (DoingQuest doingQuest : doingQuests) {
            doingQuest.changeFinishedCount(QuestDef.Condition.ITEM, item.getTemplateId(), count);
        }
    }

}
