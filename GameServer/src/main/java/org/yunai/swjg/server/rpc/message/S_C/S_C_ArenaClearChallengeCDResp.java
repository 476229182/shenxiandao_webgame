package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21806】: 增加竞技场挑战
 */
public class S_C_ArenaClearChallengeCDResp extends GameMessage {
    public static final short CODE = 21806;

    /**
     * 使用的元宝数量
     */
    private Integer useWood;
    /**
     * 当前剩余元宝
     */
    private Integer gold;

    public S_C_ArenaClearChallengeCDResp() {
    }

    public S_C_ArenaClearChallengeCDResp(Integer useWood, Integer gold) {
        this.useWood = useWood;
        this.gold = gold;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getUseWood() {
		return useWood;
	}

	public void setUseWood(Integer useWood) {
		this.useWood = useWood;
	}
	public Integer getGold() {
		return gold;
	}

	public void setGold(Integer gold) {
		this.gold = gold;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ArenaClearChallengeCDResp struct = new S_C_ArenaClearChallengeCDResp();
            struct.setUseWood(byteArray.getInt());
            struct.setGold(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ArenaClearChallengeCDResp struct = (S_C_ArenaClearChallengeCDResp) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getUseWood());
            byteArray.putInt(struct.getGold());
            return byteArray;
        }
    }
}