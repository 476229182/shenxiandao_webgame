package org.yunai.swjg.server.core.config;

/**
 * 全局共享的常量
 * User: yunai
 * Date: 13-4-23
 * Time: 下午5:27
 */
public class SharedConstants {

    /**
     * GameServer的心跳间隔,单位为毫秒
     */
    public static final int GS_HEART_BEAT_INTERVAL = 1000;
    /**
     * GameServer将double类型数据放大的倍数<br />
     * AS3对double支持不好
     */
    public static final int DOUBLE_FACTOR = 1000;
}
