package org.yunai.swjg.server.module.scene.callback;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.annotation.MainThread;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.player.PlayerService;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家下线的场景回调
 * User: yunai
 * Date: 13-4-26
 * Time: 下午7:38
 */
public class PlayerOfflineNormalSceneCallback implements SceneCallable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.player, PlayerOfflineNormalSceneCallback.class);

    private static PlayerService playerService;
    private static OnlineContextService onlineContextService;
    static {
        playerService = BeanManager.getBean(PlayerService.class);
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
    }

    /**
     * 玩家编号
     */
    private final Integer playerId;

    public PlayerOfflineNormalSceneCallback(Integer playerId) {
        this.playerId = playerId;
    }

    /**
     * 重新调用Online下线
     */
    @Override
    @MainThread
    public void call() {
        Online online = onlineContextService.getPlayer(this.playerId);
        if (online == null) {
            LOGGER.error("[call] [online:{} is not found].", this.playerId);
            return;
        }
        // 继续下线流程
        playerService.onPlayerLogout(online);
    }
}