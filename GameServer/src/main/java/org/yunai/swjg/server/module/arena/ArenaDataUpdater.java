package org.yunai.swjg.server.module.arena;

import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.updater.AbstractDataUpdater;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import java.util.HashMap;
import java.util.Map;

/**
 * 竞技场数据更新期
 * User: yunai
 * Date: 13-5-27
 * Time: 上午12:07
 */
public class ArenaDataUpdater extends AbstractDataUpdater{
    public static final Map<Class<? extends PersistenceObject>, PersistenceObjectUpdater> updaters = new HashMap<>();
    static {
    }

    @Override
    protected void doUpdate(PersistenceObject po) {
        updaters.get(po.getClass()).save(po);
    }

    @Override
    protected void doDelete(PersistenceObject po) {
        updaters.get(po.getClass()).delete(po);
    }
}
