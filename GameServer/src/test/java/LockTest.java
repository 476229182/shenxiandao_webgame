import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 锁测试类
 * User: yunai
 * Date: 13-5-13
 * Time: 上午10:24
 */
public class LockTest {

    public static void main(String[] args) {

        final Lock lock = new ReentrantLock();
//        final Map<Integer, List<Object>> map = new HashMap<>();
        final Map<Integer, List<Integer>> map = new ConcurrentHashMap<>();

        final Integer key = 1;

        Runnable runner = new Runnable() {
            @Override
            public void run() {
                List<Integer> list = map.get(key);
                Object findObj = null;
                if (list != null && !list.isEmpty()) {
                    findObj = list.get(0);
                }
                if (findObj == null) {
                    try {
                        lock.lock();
                        Thread.sleep(10000L / 2);
                        System.out.println(JSON.toJSONString(map.get(key)));
                        if (list == null) {
                            list = new ArrayList<>();
                            map.put(key, list);
                        }
                        list.add(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        };

        for (int i = 0; i < 10; i++) {
            new Thread(runner).start();
        }
    }
}
