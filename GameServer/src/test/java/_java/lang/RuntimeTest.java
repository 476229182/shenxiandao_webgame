package _java.lang;

/**
 * Runtime测试类
 * User: yunai
 * Date: 13-4-27
 * Time: 上午1:11
 */
public class RuntimeTest {

//    @Test
    public static void testAddShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Test");
            }
        });
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        testAddShutdownHook();
    }
}
