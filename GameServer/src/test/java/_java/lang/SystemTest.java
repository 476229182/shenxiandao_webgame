package _java.lang;

/**
 * System测试类
 * User: yunai
 * Date: 13-4-27
 * Time: 下午5:27
 */
public class SystemTest {

    private static class MillisecondClock {
        private long rate = 0;// 频率
        private volatile long now = 0;// 当前时间

        private MillisecondClock(long rate) {
            this.rate = rate;
            this.now = System.currentTimeMillis();
            start();
        }

        private void start() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(rate);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        now = System.currentTimeMillis();
                    }
                }
            }).start();
        }

        public long now() {
            return now;
        }

        public static final MillisecondClock CLOCK = new MillisecondClock(50);
    }

    private static class ClockTest {
        static final int THREAD_SIZE = 100;// 100线程
        static final int CALL_COUNT = 100000000;// 一亿次

        public static final MillisecondClock CLOCK = new MillisecondClock(10);

        Thread[] threads = new Thread[THREAD_SIZE];

        public void callSystem() throws InterruptedException {
            run(false);
        }

        public void callClock() throws InterruptedException {
            run(true);
        }

        private void run(final boolean isClock) throws InterruptedException {
            long s = System.currentTimeMillis();
            for (int i = 0; i < THREAD_SIZE; i++) {
                threads[i] = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int k = 0; k < CALL_COUNT; k++) {
                            // 必须赋值哦，不然编译器自动忽略
                            long l = isClock ? CLOCK.now() : System.currentTimeMillis();
                        }
                    }
                });
                threads[i].start();
            }

            // 等待完成
            for (Thread t : threads) {
                t.join();
            }
            System.out.println((isClock ? "调用时钟耗时" : "调用系统耗时") + (System.currentTimeMillis() - s) + "毫秒");
        }
    }

    /**
     * 验证{@link System#currentTimeMillis()}的效率在高并发下效率不高，和缓存时间对比<br />
     * @throws InterruptedException
     */
    public static void testCurrentTimeMillis() throws InterruptedException {
        // 切换下面注释来测试
//        new ClockTest().callClock(); // 4000ms左右
        new ClockTest().callSystem(); // 44000ms左右
    }

    public static void main(String[] args) throws InterruptedException {
        testCurrentTimeMillis();
    }
}
