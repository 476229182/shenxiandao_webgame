package org.yunai.yfserver.message;

/**
 * 消息类型
 * User: yunai
 * Date: 13-3-26
 * Time: 下午11:20
 */
public class MessageType {

    public static final short SYS_SESSION_OPEN = 1;
    public static final short SYS_SESSION_CLOSE = 2;


    public static final short SCHD_ASYNC_FINISH = 10;
    /** 调度停止 */
    public static final short SCHD_STOP = 11;
}
