package org.yunai.yfserver.session;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Session管理器
 * User: yunai
 * Date: 13-3-27
 * Time: 上午11:09
 */
public class SessionManager<S extends ISession> {

    private List<S> sessions = new CopyOnWriteArrayList<S>();

    public boolean addSession(S session) {
        return sessions.add(session);
    }

    public boolean removeSession(S session) {
        return sessions.remove(session);
    }
}
