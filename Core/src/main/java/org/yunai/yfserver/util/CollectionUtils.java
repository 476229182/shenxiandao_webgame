
package org.yunai.yfserver.util;

import java.util.*;

/**
 * 集合工具类
 * User: yunai
 * Date: 13-4-23
 * Time: 下午4:07
 */
public class CollectionUtils {

    /**
     * 将元素数组转化为HashSet
     *
     * @param ts  元素数组
     * @param <E> 元素类型
     * @return {@link HashSet<E>}
     */
    public static <E> Set<E> asSet(E... ts) {
        if (ts.length == 0) {
            return Collections.emptySet();
        }
        Set<E> set = new HashSet<E>(ts.length);
        Collections.addAll(set, ts);
        return set;
    }

    /**
     * 将元素数组转化为ArrayList
     *
     * @param ts  元素数组
     * @param <E> 元素类型
     * @return {@link ArrayList<E>}
     */
    public static <E> List<E> asList(E... ts) {
        if (ts.length == 0) {
            return Collections.emptyList();
        }
        List<E> list = new ArrayList<>(ts.length);
        Collections.addAll(list, ts);
        return list;
    }

    /**
     * 判断数组是否为空
     *
     * @param collection 集合
     * @param <E>        元素类型
     * @return 是否问空
     */
    public static <E> boolean isEmpty(Collection<E> collection) {
        return collection == null || collection.isEmpty();
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> emptySet(Class<T> clazz) {
        return (Set<T>) Collections.EMPTY_SET;
    }

}
